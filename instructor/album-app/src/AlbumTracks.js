import React from "react";

const AlbumTracks = (props) => {
	return (
		props.visible && (
			<ol className="album-tracks">
				{props.tracks.map((track) => (
					<li key={track}>{track}</li>
				))}
			</ol>
		)
	);
};

export default AlbumTracks;
