import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";

import Greet from "./Greet";
import Person from "./Person";

const PersonFunc = ({ name }) => {
	const [height, setHeight] = useState(185);
	const [weight, setWeight] = useState(200);

	useEffect(() => {
		console.log("it also rendered");
		setHeight(height + 1);
	}, []); // like componentDidMount

	useEffect(() => {
		if (height !== 185 || weight !== 200) {
			console.log("it also updated");
		}
	}, [height, weight]); // like componentDidUpdate

	return (
		<div>
			<Greet person={name} name="AllState" height={height} weight={weight} />
			<p>
				{name} weighs {weight}lbs and is {height}
				cm tall.
			</p>
			<button onClick={() => setWeight(weight + 1)}>Grow weight</button>
			<button onClick={() => setHeight(height + 1)}>Grow height</button>
		</div>
	);
};

const main = (
	<React.Fragment>
		<Person name="John" />
		<PersonFunc name="May" />
	</React.Fragment>
);

ReactDOM.render(main, document.querySelector("#root"));
