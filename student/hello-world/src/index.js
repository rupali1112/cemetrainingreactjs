import React from 'react';
import ReactDOM from 'react-dom';

const header = React.createElement("h1", null, "Header");

const head1 = React.createElement("p", {className: "hello-text", id: "hello-id"}, "Hello World!!");
const main = React.createElement( "div", { id: "ice-cream" }, [
    React.createElement( "h1", null, "Ice Cream Flavours" ),
    React.createElement( "ul", { class: "ice-cream-list" }, [
    React.createElement( "li", null, "Vanilla" ),
    React.createElement( "li", null, "Chocolate" ),
    React.createElement( "li", null, "Raspberry Ripple" )
    ] )
    ] );
ReactDOM.render(main,document.getElementById("root"));
